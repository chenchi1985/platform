FROM registry.cn-zhangjiakou.aliyuncs.com/cytong/tomcat8:latest

RUN rm -rf /usr/local/tomcat/webapps/ROOT
COPY ./platform-framework/target/platform-framework.war /usr/local/tomcat/webapps/ROOT.war